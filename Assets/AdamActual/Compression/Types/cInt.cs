#define ADAMACTUAL_DEBUG

using System;
using System.Linq;

namespace AdamActual.Compression {
    public struct cInt : ICompressedNumber {
        readonly int _minValue;
        public int MinValue { get { return _minValue; } }

        readonly int _maxValue;
        public int MaxValue { get { return _maxValue; } }

        readonly int _range;
        public int Range { get { return _range; } }

        readonly int _bitCount;
        public int BitCount { get { return _bitCount; } }

        int _value;
        public int Value {
            get { return _value; }
            set { _value = GetClampedValue(value, _minValue, _maxValue); }
        }

        public cInt(int value, int minValue, int maxValue) : this(minValue, maxValue) {
            _value = GetClampedValue(value, _minValue, _maxValue);
        }

        public cInt(int minValue, int maxValue) {
            _minValue = minValue;
            _maxValue = maxValue;

            _range = GetRange(minValue, maxValue);

            _bitCount = GetBitCount(minValue, maxValue);

            _value = minValue;
        }

        /// <inheritdoc cref="cInt(Type)"/>
        public cInt(Type enumType, int value) : this(enumType) {
            _value = value;
        }

        /// <summary>
        /// <para>Create a <see cref="cInt"/> using the <see cref="MinValue"/> and <see cref="MaxValue"/> of <see cref="enumType"/>'s integer values.</para>
        /// Note: Additional bits will be used if <see cref="enumType"/>'s values are non-consecutive. (<see cref="Range"/> = <see cref="enumType"/>.MaxValue - <see cref="enumType"/>.MinValue + 1.)
        /// </summary>
        public cInt(Type enumType) {
            _minValue = int.MaxValue;
            _maxValue = int.MinValue;

            foreach (int value in Enum.GetValues(enumType).Cast<int>()) {
                if (value < _minValue) { _minValue = value; }
                if (value > _maxValue) { _maxValue = value; }
            }

            _range = GetRange(_minValue, _maxValue);

            _bitCount = GetBitCount(_minValue, _maxValue);

            _value = _minValue;
        }

        static int GetRange(int min, int max) {
            return max - min + 1;
        }

        static int GetBitCount(int min, int max) {
            return GetBitCount(max - min);
        }

        static int GetBitCount(int value) {
            return value == 0 ? 1 : (int)(Math.Log(value, 2) + 1);
        }

        static int GetClampedValue(int value, int minValue, int maxValue) {
#if ADAMACTUAL_DEBUG
            if (value < minValue || value > maxValue) { UnityEngine.Debug.LogWarningFormat("Clamping value [{0}] to range [{1}, {2}].", value, minValue, maxValue); }
#endif

            if (value < minValue) { return minValue; }
            if (value > maxValue) { return maxValue; }
            return value;
        }

        public static implicit operator int(cInt cInt) {
            return cInt._value;
        }
    }
}