using System;
using System.Collections.Generic;

namespace AdamActual.Events {
    /// <summary>Prevent external classes invoking <see cref="Event.Raise"/> by exposing <see cref="IEvent"/> instead.</summary>
    public abstract class EventBase {
    }

    /// <inheritdoc cref="EventBase" />
    public sealed class Event : EventBase, IEvent {
        readonly List<Action> _listeners = new List<Action>();

        public void AddListener(Action listener) { _listeners.Add(listener); }
        public void RemoveListener(Action listener) { _listeners.Remove(listener); }
        public void ClearListeners() { _listeners.Clear(); }

        public void Raise() {
            for (int i = _listeners.Count - 1; i >= 0; i--) { _listeners[i](); }
        }
    }

    /// <inheritdoc cref="EventBase" />
    public sealed class Event<T> : EventBase, IEvent<T> {
        readonly List<Action<T>> _listeners = new List<Action<T>>();

        public void AddListener(Action<T> listener) { _listeners.Add(listener); }
        public void RemoveListener(Action<T> listener) { _listeners.Remove(listener); }
        public void ClearListeners() { _listeners.Clear(); }

        public void Raise(T arg0) {
            for (int i = _listeners.Count - 1; i >= 0; i--) { _listeners[i](arg0); }
        }
    }

    /// <inheritdoc cref="EventBase" />
    public sealed class Event<T1,T2> : EventBase, IEvent<T1,T2> {
        readonly List<Action<T1,T2>> _listeners = new List<Action<T1,T2>>();

        public void AddListener(Action<T1,T2> listener) { _listeners.Add(listener); }
        public void RemoveListener(Action<T1,T2> listener) { _listeners.Remove(listener); }
        public void ClearListeners() { _listeners.Clear(); }

        public void Raise(T1 arg0, T2 arg1) {
            for (int i = _listeners.Count - 1; i >= 0; i--) { _listeners[i](arg0, arg1); }
        }
    }

    /// <inheritdoc cref="EventBase" />
    public sealed class Event<T1,T2,T3> : EventBase, IEvent<T1,T2,T3> {
        readonly List<Action<T1,T2,T3>> _listeners = new List<Action<T1,T2,T3>>();

        public void AddListener(Action<T1,T2,T3> listener) { _listeners.Add(listener); }
        public void RemoveListener(Action<T1,T2,T3> listener) { _listeners.Remove(listener); }
        public void ClearListeners() { _listeners.Clear(); }

        public void Raise(T1 arg0, T2 arg1, T3 arg2) {
            for (int i = _listeners.Count - 1; i >= 0; i--) { _listeners[i](arg0, arg1, arg2); }
        }
    }

    /// <inheritdoc cref="EventBase" />
    public sealed class Event<T1,T2,T3,T4> : EventBase, IEvent<T1,T2,T3,T4> {
        readonly List<Action<T1,T2,T3,T4>> _listeners = new List<Action<T1,T2,T3,T4>>();

        public void AddListener(Action<T1,T2,T3,T4> listener) { _listeners.Add(listener); }
        public void RemoveListener(Action<T1,T2,T3,T4> listener) { _listeners.Remove(listener); }
        public void ClearListeners() { _listeners.Clear(); }

        public void Raise(T1 arg0, T2 arg1, T3 arg2, T4 arg3) {
            for (int i = _listeners.Count - 1; i >= 0; i--) { _listeners[i](arg0, arg1, arg2, arg3); }
        }
    }
}