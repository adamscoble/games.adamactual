using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace AdamActual.Compression {
    public class CompressionBuffer {
        const byte BYTE_ZERO = 0;
        const int BYTE_BITS = 8;

        readonly bool _isReading;

        readonly byte[] _bytesRead;
        readonly List<byte> _bytesWrite;

        public int ByteCount { get { return _isReading ? _bytesRead.Length : _bytesWrite.Count; } }
        public int BitCount { get { return ByteCount * BYTE_BITS; } }

        int _currentBitIndex;

        public CompressionBuffer(string path) : this(File.ReadAllBytes(path)) {

        }

        public CompressionBuffer(byte[] bytes) {
            _isReading = true;
            _bytesRead = bytes;
        }

        public CompressionBuffer() {
            _bytesWrite = new List<byte> { 0 };
        }

#region Read
        public bool ReadBool() {
            return IsValidRead(1) && IsBitAtIndexSet(_currentBitIndex++);
        }

        public void Read(ref ICompressedNumber value) {
            if (!IsValidRead(value.BitCount)) { return; }

            value.Value = value.MinValue + ReadInt(value.BitCount);
        }

        static bool IsValidRead(int bits) {
            return bits != 0;
        }

        int ReadInt(int bits) {
            int value = 0;

            for (int i = 0; i < bits; i++) {
                if(IsBitAtIndexSet(_currentBitIndex)) { value |= 1 << i; }

                _currentBitIndex++;
            }

            return value;
        }

        bool IsBitAtIndexSet(int bitIndex) {
            return (_isReading ?
                       _bytesRead[bitIndex / BYTE_BITS] & (1 << (bitIndex % BYTE_BITS)) :
                       _bytesWrite[bitIndex / BYTE_BITS] & (1 << (bitIndex % BYTE_BITS))
                   ) != 0;
        }
#endregion

#region Write
        public void Write(bool value) {
            if (!IsValidWrite(1)) { return; }

            if(value) { SetBitAtIndex(_currentBitIndex); }

            _currentBitIndex++;
        }

        public void Write(ICompressedNumber value) {
            if (!IsValidWrite(value.BitCount)) { return; }

            WriteInt(value.Value - value.MinValue, value.BitCount);
        }

        bool IsValidWrite(int bits) {
            if (_isReading || bits == 0) { return false; }

            EnsureBitsAvailable(bits);

            return true;
        }

        void EnsureBitsAvailable(int bits) {
            int availableBits = BitCount - _currentBitIndex;

            if (bits <= availableBits) { return; }

            _bytesWrite.AddRange(Enumerable.Repeat(BYTE_ZERO, (int)Math.Ceiling((bits - availableBits) / (float)BYTE_BITS)));
        }

        void WriteInt(int value, int bits) {
            for (int i = 0; i < bits; i++) {
                if((value & (1 << i)) != 0) { SetBitAtIndex(_currentBitIndex); }

                _currentBitIndex++;
            }
        }

        void SetBitAtIndex(int bitIndex) {
            _bytesWrite[bitIndex / BYTE_BITS] |= (byte)(1 << (bitIndex % BYTE_BITS));
        }
#endregion

        public byte[] GetAllBytes() {
            return _isReading ? _bytesRead : _bytesWrite.ToArray();
        }

        public void WriteAllBytesToFile(string path) {
            File.WriteAllBytes(path, GetAllBytes());
        }

        /// <summary>Returns the current read/write index to the beginning (0). Can be used to enable reading after writing.</summary>
        public void Reset() {
            _currentBitIndex = 0;
        }
    }
}