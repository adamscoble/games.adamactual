using System;

namespace AdamActual.Events {
    public interface IEvent {
        void AddListener(Action listener);
        void RemoveListener(Action listener);
    }

    public interface IEvent<T> {
        void AddListener(Action<T> listener);
        void RemoveListener(Action<T> listener);
    }

    public interface IEvent<T1,T2> {
        void AddListener(Action<T1,T2> listener);
        void RemoveListener(Action<T1,T2> listener);
    }

    public interface IEvent<T1,T2,T3> {
        void AddListener(Action<T1,T2,T3> listener);
        void RemoveListener(Action<T1,T2,T3> listener);
    }

    public interface IEvent<T1,T2,T3,T4> {
        void AddListener(Action<T1,T2,T3,T4> listener);
        void RemoveListener(Action<T1,T2,T3,T4> listener);
    }
}