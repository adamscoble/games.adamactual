using System;
using System.Collections.Generic;

namespace AdamActual.Serialization.INI {
    internal abstract class INIProperty {
        protected static readonly Dictionary<Type, string> EnumCommentNames = new Dictionary<Type, string>();
        protected static readonly Dictionary<Type, string> EnumCommentNamesAndValues = new Dictionary<Type, string>();

        public abstract string Name { get; }
        public abstract Type Type { get; }

        public abstract bool IsArray { get; }
        public abstract bool ArraySerializeMultipleEntries { get; protected set; }
        public abstract bool IsBool { get; protected set; }
        public abstract bool IsNumber { get; protected set; }
        public abstract bool IsNonDecimalNumber { get; protected set; }
        public abstract bool IsEnum { get; protected set; }
        public abstract bool IsString { get; protected set; }

        public abstract string Comment { get; protected set; }
        public abstract bool HasComment { get; protected set; }
        public abstract string InlineComment { get; protected set; }
        public abstract bool HasInlineComment { get; protected set; }

        public abstract object GetValue(object obj);
        public abstract object GetArrayValue(object value);
        public abstract void SetValue(object obj, object value);
    }
}