using System;
using UnityEngine;

namespace MultiAngleBillboard {
    public class MultiAngleBillboard : MonoBehaviour {
        public Sprite[] Angles;
        int _angleCount;
        int _degreeIncrement;

        Vector3 Position2D {
            get {
                Vector2 position = transform.localPosition;
                position.y = 0;
                return position;
            }
        }

        void Awake() {
            _angleCount = Angles.Length;
            _degreeIncrement = 360 / _angleCount;
        }

        void Update() {
            Vector3 cameraPosition2D = Camera.main.transform.localPosition;
            cameraPosition2D.y = 0;

            int angleIndex = GetAngleIndex(cameraPosition2D);
            Debug.LogFormat("Angle index: {0}", angleIndex);

            // TODO: Add a SpriteRenderer child, set the correct Sprite and rotate it to always face the camera
        }

        int GetAngleIndex(Vector3 viewPosition2D) {
            Vector3 directionFromViewingPosition = Position2D - viewPosition2D;
            Vector3 facingDirection = transform.forward;

            float viewAngleOfBillboard = (Vector3.SignedAngle(directionFromViewingPosition, facingDirection, Vector3.up) + 360) % 360;
            int nearestDegreeIncrement = (int)Math.Round(viewAngleOfBillboard / _degreeIncrement) * _degreeIncrement;

            return (nearestDegreeIncrement / _degreeIncrement) % _angleCount;
        }
    }
}