using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace AdamActual {
    public static class CodeProfile {
        static readonly Stopwatch _stopwatch = new Stopwatch();

        /// <summary>Tidied version of the function found here: https://stackoverflow.com/a/1048708</summary>
        public static double Profile(string label, Action action, int count) {
            action(); // Warm up

            _stopwatch.Reset();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            _stopwatch.Start();
            for (int i = 0; i < count; i++) { action(); }
            _stopwatch.Stop();

            double time = _stopwatch.Elapsed.TotalMilliseconds;

            Debug.LogFormat("{0}\nTime Elapsed {1}ms", label, time);

            return time;
        }
    }
}