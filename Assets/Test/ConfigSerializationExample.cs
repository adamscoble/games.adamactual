using System.IO;
using UnityEngine;
using AdamActual.Serialization.INI;

public enum ExampleEnum { First, Second, Third }

[INI(Sections = new [] { "Second section", "Bob" }, SectionComments = new []{ "Second section comment"}, SectionInlineComments = new []{"Second section inline comment"})]
public class ConfigSerializationExample : MonoBehaviour {
    [SerializeField] string _first;
    [INIProperty(Section = "First section",
                    Comment = "A comment on the first property",
                    Name = "FirstNameOverride")] public string First { get { return _first; } set { _first = value; } }

    [SerializeField] string _second;
    [INIProperty(Name = "SecondNameOverride")] public string Second { get { return _second; } set { _second = value; } }

    [SerializeField] string _third;
    [INIProperty(SectionIndex = 1, Comment = "Hey there I'm third")] public string Third { get { return _third; } set { _third = value; } }

    [SerializeField] string _fourth;
    [INIProperty] public string Fourth { get { return _fourth; } set { _fourth = value; } }

    [SerializeField] string _fifth;
    [INIProperty(Comment = "A comment on the fifth property")] public string Fifth { get { return _fifth; } set { _fifth = value; } }

    [SerializeField] float _sixth;
    [INIProperty] public float Sixth { get { return _sixth; } set { _sixth = value; } }

    [SerializeField] bool _seventh;
    [INIProperty(BoolSerializeAsString = INIPropertyBool.True)] public bool Seventh { get { return _seventh; } set { _seventh = value; } }

    [SerializeField] ExampleEnum _eighth;
    [INIProperty(Section = "Third section")] public ExampleEnum Eighth { get { return _eighth; } set { _eighth = value; } }

    [SerializeField] string[] _ninth;
    [INIProperty(InlineComment = "I am an array")] public string[] Ninth { get { return _ninth; } set { _ninth = value; } }

    [SerializeField] ExampleEnum[] _tenth;
    [INIProperty(Section = "First section", EnumSerializeAsName = INIPropertyBool.False, ArraySerializeMultipleEntries = INIPropertyBool.True)] public ExampleEnum[] Tenth { get { return _tenth; } set { _tenth = value; } }

    [SerializeField] ExampleEnum _eleventh;
    [INIProperty(InlineComment = "Howdy I'm 11!")] public ExampleEnum Eleventh { get { return _eleventh; } set { _eleventh = value; } }

    void Update() {
        if (Input.GetKeyDown(KeyCode.S)) {
            File.WriteAllText(Path.Combine(Application.persistentDataPath, "Ini.ini"), INI.Serialize(this));
        } else if (Input.GetKeyDown(KeyCode.L)) {
            INI.Deserialize(File.ReadAllText(Path.Combine(Application.persistentDataPath, "Ini.ini")), this);
        }
    }
}
