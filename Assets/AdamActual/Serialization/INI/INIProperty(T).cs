using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AdamActual.Serialization.INI {
    internal sealed class INIProperty<T> : INIProperty where T : MemberInfo {
        INIAttribute INIAttribute { get; }
        INIPropertyAttribute PropertyAttribute { get; }

        bool IsProperty { get; }
        PropertyInfo Property { get; }
        FieldInfo Field { get; }

        public override string Name { get; }
        public override Type Type { get; }

        public override bool IsArray { get; }
        public override bool ArraySerializeMultipleEntries { get; protected set; }
        public override bool IsBool { get; protected set; }
        public override bool IsNumber { get; protected set; }
        public override bool IsNonDecimalNumber { get; protected set; }
        public override bool IsEnum { get; protected set; }
        public override bool IsString { get; protected set; }

        public override string Comment { get; protected set; }
        public override bool HasComment { get; protected set; }
        public override string InlineComment { get; protected set; }
        public override bool HasInlineComment { get; protected set; }

        bool CastValueToInt { get; set; }
        bool CastArrayValueToInt { get; set; }

        public INIProperty(INIAttribute iniAttribute, T property, INIPropertyAttribute propertyAttribute) {
            INIAttribute = iniAttribute;
            PropertyAttribute = propertyAttribute;

            Property = property as PropertyInfo;
            IsProperty = Property != null;
            if (!IsProperty) { Field = property as FieldInfo; }

            Name = propertyAttribute?.Name ?? property.Name;
            Type type = IsProperty ? Property.PropertyType : Field.FieldType;
            IsArray = type.IsArray;

            Type = IsArray ? type.GetElementType() : type;

            InitType();

            Comment = Comment ?? GetComment(propertyAttribute?.Comment, null);
            HasComment = Comment != null;
            InlineComment = InlineComment ?? GetComment(propertyAttribute?.InlineComment, null);
            HasInlineComment = InlineComment != null;
        }

        void InitType() {
            if (IsArray) { ArraySerializeMultipleEntries = GetConfigPropertySetting(PropertyAttribute?.ArraySerializeMultipleEntries, INIAttribute.ArraySerializeMultipleEntries); }

            if (IsBool = Type == typeof(bool)) {
                InitCastValueToInt(PropertyAttribute?.BoolSerializeAsString, INIAttribute.BoolSerializeAsString);

                return;
            }

            if (IsEnum = Type.IsEnum) {
                InitCastValueToInt(PropertyAttribute?.EnumSerializeAsName, INIAttribute.EnumSerializeAsName);

                bool appendValues = GetConfigPropertySetting(PropertyAttribute?.EnumCommentAppendValues, INIAttribute.EnumCommentAppendValues);
                bool appendInline = appendValues && GetConfigPropertySetting(PropertyAttribute?.EnumCommentAppendInline, INIAttribute.EnumCommentAppendInline);
                Comment = GetComment(PropertyAttribute?.Comment, appendValues && !appendInline ? GetEnumCommentAppendValues() : null);
                InlineComment = GetComment(PropertyAttribute?.InlineComment, appendInline ? GetEnumCommentAppendValues() : null);

                return;
            }

            bool isNonDecimalNumber = false;
            IsNumber = IsNumeric(ref isNonDecimalNumber);
            if (IsNonDecimalNumber = isNonDecimalNumber || IsNumber) { return; }

            if (IsString = Type == typeof(string)) { return; }
        }

        void InitCastValueToInt(INIPropertyBool? setting, bool globalValue) {
            bool serializeAsString = GetConfigPropertySetting(setting, globalValue);
            CastValueToInt = !IsArray && !serializeAsString;
            CastArrayValueToInt = IsArray && !serializeAsString;
        }

        static bool GetConfigPropertySetting(INIPropertyBool? setting, bool globalValue) {
            return !setting.HasValue || setting == INIPropertyBool.Inherit ? globalValue : setting == INIPropertyBool.True;
        }

        bool IsNumeric(ref bool isNonDecimal) {
            switch (Type.GetTypeCode(Type)) {
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64: return isNonDecimal = true;
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal: return true;
                default: return false;
            }
        }

        static string GetComment(string comment, string append) {
            comment = comment ?? "";

            if (!string.IsNullOrEmpty(append)) { comment += append; }

            return comment.Length > 0 ? comment : null;
        }

        string GetEnumCommentAppendValues() {
            Dictionary<Type, string> cache = CastValueToInt ? EnumCommentNamesAndValues : EnumCommentNames;
            return cache.ContainsKey(Type) ? cache[Type] : cache[Type] = string.Join(",", CastValueToInt ? Enum.GetNames(Type).Select(name => $"{name}={(int)Enum.Parse(Type, name)}") : Enum.GetNames(Type));
        }

        public override object GetValue(object obj) {
            return GetValue(CastValueToInt, IsProperty ? Property.GetValue(obj) : Field.GetValue(obj));
        }

        public override object GetArrayValue(object value) {
            return GetValue(CastArrayValueToInt, value);
        }

        object GetValue(bool castValueToInt, object value) {
            if (!IsBool) { return castValueToInt ? (int)value : value; }

            if (castValueToInt) { return (bool)value ? 1 : 0; }

            return value.ToString().ToLower();
        }

        public override void SetValue(object obj, object value) {
            if (IsProperty) {
                Property.SetValue(obj, value);
            } else {
                Field.SetValue(obj, value);
            }
        }
    }
}