using System;
using System.Collections.Generic;

namespace AdamActual {
    public static class EnumCount {
        static readonly Dictionary<Type, int> _enums = new Dictionary<Type, int>();

        public static int Get<T>() {
            return Get(typeof(T));
        }

        public static int Get(Type type) {
            if (!_enums.ContainsKey(type)) { _enums[type] = Enum.GetValues(type).Length; }

            return _enums[type];
        }
    }
}