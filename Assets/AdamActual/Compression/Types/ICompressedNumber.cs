namespace AdamActual.Compression {
    public interface ICompressedNumber {
        /// <summary>The smallest possible value of this <see cref="ICompressedNumber"/>.</summary>
        int MinValue { get; }
        /// <summary>The largest possible value of this <see cref="ICompressedNumber"/>.</summary>
        int MaxValue { get; }
        /// <summary>The total possible values of this <see cref="ICompressedNumber"/>.</summary>
        int Range { get; }

        /// <summary>The amount of bits required to represent this <see cref="ICompressedNumber"/>.</summary>
        int BitCount { get; }

        /// <summary>The value of this <see cref="ICompressedNumber"/>. (Clamped to <see cref="MinValue"/> and <see cref="MaxValue"/>.)</summary>
        int Value { get; set; }
    }
}