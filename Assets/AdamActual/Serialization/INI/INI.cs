using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdamActual.Serialization.INI {
    public static class INI {
        public const AttributeTargets CLASS_ATTRIBUTE_TARGETS = AttributeTargets.Class | AttributeTargets.Struct;
        public const AttributeTargets PROPERTY_ATTRIBUTE_TARGETS = AttributeTargets.Property | AttributeTargets.Field;

        static readonly char[] _lineCommentSplit = { ';', '#' };
        static readonly char[] _deserializePropertySplit = { '=' };
        static readonly char[] _deserializeArrayTrim = { '[', ']' };
        static readonly char[] _nonDecimalNumericSplit = { '.' };

        static readonly Dictionary<INIProperty, List<string>> _deserializedArrayProperties = new Dictionary<INIProperty, List<string>>();
        static readonly Queue<List<string>> _deserializedArrayValuesPool = new Queue<List<string>>();

        public static string Serialize<T>(T value) {
            INIClass iniClass = INIClass.Get(typeof(T));
            INIPadType padType = iniClass.INIAttribute.PadType;

            StringBuilder stringBuilder = new StringBuilder();

            foreach (INISection section in iniClass.GetSections()) {
                if (section.HasName) {
                    if (stringBuilder.Length > 0) { stringBuilder.AppendLine(); }

                    if (section.HasComment) { stringBuilder.AppendLine($"; {section.Comment}"); }

                    stringBuilder.AppendLine($"[{section.Name}]{(section.HasInlineComment ? $" ; {section.InlineComment}" : "")}");
                }

                foreach (INIProperty property in section.GetProperties()) {
                    if (property.HasComment) { stringBuilder.AppendLine($"; {property.Comment}"); }

                    string format = padType == INIPadType.None ? "{0}={1}{2}" : "{0} = {1}{2}";
                    string propertyName = GetPropertyNameSerialize(padType, property.Name, section.MaxPropertyNameLength, iniClass.MaxPropertyNameLength);
                    string inlineComment = property.HasInlineComment ? $" ; {property.InlineComment}" : "";

                    if (property.ArraySerializeMultipleEntries) {
                        string[] arrayValues = GetPropertyArrayValuesSerialize(property, value);
                        for (int i = arrayValues.Length > 0 ? 0 : -1; i < arrayValues.Length; i++) { stringBuilder.AppendLine(string.Format(format, propertyName, i != -1 ? arrayValues[i] : "", i < 1 ? inlineComment : null)); }
                    } else {
                        stringBuilder.AppendLine(string.Format(format, propertyName, GetPropertyValueSerialize(property, value), inlineComment));
                    }
                }
            }

            return stringBuilder.ToString();
        }

        static string GetPropertyNameSerialize(INIPadType padType, string name, int sectionMaxLength, int globalMaxLength) {
            return padType == INIPadType.None || padType == INIPadType.Spaced ? name : name.PadRight(padType == INIPadType.Section ? sectionMaxLength : globalMaxLength);
        }

        static string[] GetPropertyArrayValuesSerialize<T>(INIProperty property, T value) {
            Array propertyValue = (Array)property.GetValue(value);
            string[] arrayValues = new string[propertyValue.Length];

            int index = 0;
            foreach (object arrayValue in propertyValue) { arrayValues[index++] = property.GetArrayValue(arrayValue).ToString(); }

            return arrayValues;
        }

        static string GetPropertyValueSerialize<T>(INIProperty property, T value) {
            object propertyValue = property.GetValue(value);

            if (!property.IsArray) { return propertyValue?.ToString() ?? ""; }

            StringBuilder stringBuilder = new StringBuilder("[");

            foreach (object arrayValue in (Array)propertyValue) {
                if (stringBuilder.Length > 1) { stringBuilder.Append(","); }

                stringBuilder.Append(property.GetArrayValue(arrayValue));
            }

            return stringBuilder.Append("]").ToString();
        }

        public static T Deserialize<T>(string value) where T : new() {
            T target = new T();

            Deserialize(value, target);

            return target;
        }

        public static void Deserialize<T>(string value, T target) {
            if (string.IsNullOrEmpty(value)) { return; }

            INIClass iniClass = INIClass.Get(typeof(T));
            INISection section;
            bool isValidSection;
            GetSection(iniClass, "", out section, out isValidSection);

            IEnumerable<string> lines = value.Split('\n');

            _deserializedArrayProperties.Clear();

            foreach (string l in lines) {
                string line = l.Split(_lineCommentSplit, 2)[0].Trim();

                if (line.Length == 0) { continue; }

                if (line[0] == '[') {
                    GetSection(iniClass, line.Substring(1, line.Length - 2), out section, out isValidSection);

                    ProcessDeserializedArrayProperties(target);

                    continue;
                }

                if (!isValidSection) { continue; }

                string[] propertyNameValue = line.Split(_deserializePropertySplit, 2);

                if (propertyNameValue.Length != 2) { continue; }

                string propertyName = propertyNameValue[0].TrimEnd();
                string propertyValue = propertyNameValue[1].TrimStart();

                if (propertyName.Length == 0) { continue; }

                INIProperty property = section.GetProperty(propertyName);

                if (property == null) {
                    UnityEngine.Debug.LogError($"Property {propertyName} not found{(section.Name.Length > 0 ? $" in section {section.Name}" : "")}, ignoring");

                    continue;
                }

                if (property.IsArray) {
                    if (!_deserializedArrayProperties.ContainsKey(property)) { _deserializedArrayProperties[property] = _deserializedArrayValuesPool.Count > 0 ? _deserializedArrayValuesPool.Dequeue() : new List<string>(); };

                    _deserializedArrayProperties[property].Add(propertyValue);

                    continue;
                }

                SetPropertyValue(target, property, propertyName, GetPropertyValueDeserialize(property, propertyValue));
            }

            ProcessDeserializedArrayProperties(target);
        }

        static void SetPropertyValue<T>(T target, INIProperty property, string propertyName, object deserializedPropertyValue) {
            try {
                property.SetValue(target, deserializedPropertyValue);
            } catch (Exception exception) {
                UnityEngine.Debug.LogError($"Incorrect value for property {propertyName}, ignoring ({exception.Message})");
            }
        }

        static void ProcessDeserializedArrayProperties<T>(T target) {
            foreach (KeyValuePair<INIProperty, List<string>> arrayProperty in _deserializedArrayProperties) {
                SetPropertyValue(target, arrayProperty.Key, arrayProperty.Key.Name, GetPropertyArrayValueDeserialize(arrayProperty.Key, arrayProperty.Value));

                arrayProperty.Value.Clear();
                _deserializedArrayValuesPool.Enqueue(arrayProperty.Value);
            }

            _deserializedArrayProperties.Clear();
        }

        static void GetSection(INIClass iniClass, string name, out INISection section, out bool isValid) {
            section = iniClass.GetSection(name);
            isValid = section != null;

            if (!isValid) { UnityEngine.Debug.LogError($"Section {name} not found, ignoring"); }
        }

        static object GetPropertyValueDeserialize(INIProperty property, string value) {
            if (property.IsBool) { return value.Length == 1 ? value == "1" : Convert.ChangeType(value, property.Type); }
            if (property.IsString) { return value; }
            if (property.IsEnum) { return Enum.Parse(property.Type, value); }

            if (property.IsNonDecimalNumber) { value = value.Split(_nonDecimalNumericSplit, 1)[0]; }

            return Convert.ChangeType(value, property.Type);
        }

        static object GetPropertyArrayValueDeserialize(INIProperty property, List<string> values) {
            string[] arrayValues = values.Count > 1 ? values.ToArray() : values[0].Trim(_deserializeArrayTrim).Split(',').Select(arrayValue => arrayValue.Trim()).Where((arrayValue, index) => index > 0 || arrayValue.Length > 0).ToArray();
            Array array = Array.CreateInstance(property.Type, arrayValues.Length);

            for (int i = 0; i < arrayValues.Length; i++) {
                if (arrayValues[i] == "") { continue; }

                array.SetValue(GetPropertyValueDeserialize(property, arrayValues[i]), i);
            }

            return array;
        }
    }
}