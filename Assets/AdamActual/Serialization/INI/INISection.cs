using System;
using System.Collections.Generic;
using System.Reflection;

namespace AdamActual.Serialization.INI {
    internal sealed class INISection {
        INIAttribute INIAttribute { get; }

        public string Name { get; }
        public bool HasName { get; }
        public int Order { get; }

        public string Comment { get; }
        public bool HasComment { get; }
        public string InlineComment { get; }
        public bool HasInlineComment { get; }

        readonly List<INIProperty> _properties = new List<INIProperty>();
        readonly Dictionary<string, INIProperty> _propertiesByName = new Dictionary<string, INIProperty>();
        public int MaxPropertyNameLength;

        public INISection(INIAttribute iniAttribute, string name, int order = int.MaxValue, string comment = null, string inlineComment = null) {
            INIAttribute = iniAttribute;

            Name = name;
            HasName = name != "";
            Order = order;

            Comment = string.IsNullOrEmpty(comment) ? null : comment;
            HasComment = Comment != null;
            InlineComment = string.IsNullOrEmpty(inlineComment) ? null : inlineComment;
            HasInlineComment = InlineComment != null;
        }

        public void AddProperty<T>(T property, INIPropertyAttribute attribute) where T : MemberInfo {
            INIProperty iniProperty = new INIProperty<T>(INIAttribute, property, attribute);
            MaxPropertyNameLength = Math.Max(iniProperty.Name.Length, MaxPropertyNameLength);

            _properties.Add(iniProperty);
            _propertiesByName.Add(iniProperty.Name, iniProperty);
        }

        public IEnumerable<INIProperty> GetProperties() {
            return _properties;
        }

        public INIProperty GetProperty(string name) {
            return _propertiesByName.ContainsKey(name) ? _propertiesByName[name] : null;
        }
    }
}