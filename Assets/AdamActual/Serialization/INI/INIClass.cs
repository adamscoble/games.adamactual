using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AdamActual.Serialization.INI {
    internal sealed class INIClass {
        const BindingFlags DECLARED_ONLY = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;

        static readonly Dictionary<Type, INIClass> _cache = new Dictionary<Type, INIClass>();

        public INIAttribute INIAttribute { get; }

        readonly Dictionary<string, INISection> _sections = new Dictionary<string, INISection>();
        IOrderedEnumerable<INISection> _sectionsOrdered;
        IOrderedEnumerable<INISection> SectionsOrdered => _sectionsOrdered ?? (_sectionsOrdered = _sections.Values.OrderBy(section => section.Name != "").ThenBy(section => section.Order));
        public int MaxPropertyNameLength { get; }

        public static INIClass Get(Type type) {
            return _cache.ContainsKey(type) ? _cache[type] : _cache[type] = new INIClass(type);
        }

        INIClass(Type type) {
            INIAttribute = type.GetCustomAttribute<INIAttribute>(false) ?? new INIAttribute();

            InitINIAttributeSections();
            IEnumerable<PropertyInfo> properties = type.GetProperties(DECLARED_ONLY).Where(info => IsValidProperty(INIAttribute.Properties, info));
            IEnumerable<FieldInfo> fields = type.GetFields(DECLARED_ONLY).Where(info => IsValidProperty(INIAttribute.Fields, info));

            MaxPropertyNameLength = Math.Max(InitProperties(properties), InitProperties(fields));
        }

        bool IsValidProperty(bool includeMemberType, MemberInfo info) {
            if (info.GetCustomAttribute<INIPropertyAttribute>(false) != null) { return true; }

            return INIAttribute.MemberSerialization == MemberSerialization.OptOut && includeMemberType && info.GetCustomAttribute<INIIgnoreAttribute>(false) == null;
        }

        void InitINIAttributeSections() {
            INIAttribute.Sections = INIAttribute.Sections ?? new string[0];

            if (INIAttribute.Sections.Length == 0) { return; }

            bool isComments = INIAttribute.SectionComments != null;
            bool isInlineComments = INIAttribute.SectionInlineComments != null;

            for (int i = 0; i < INIAttribute.Sections.Length; i++) {
                string name = INIAttribute.Sections[i].Trim();

                if (name.Length == 0) { continue; }

                _sections[name] = new INISection(
                    INIAttribute,
                    name,
                    i,
                    isComments && i < INIAttribute.SectionComments.Length ? INIAttribute.SectionComments[i].Trim() : null,
                    isInlineComments && i < INIAttribute.SectionInlineComments.Length ? INIAttribute.SectionInlineComments[i].Trim() : null
                );
            }
        }

        int InitProperties<T>(IEnumerable<T> properties) where T : MemberInfo {
            int maxPropertyNameLength = 0;

            foreach (T property in properties) {
                INIPropertyAttribute attribute = property.GetCustomAttribute<INIPropertyAttribute>(false);

                string sectionName = attribute == null ? "" : attribute.Section ?? (attribute.SectionIndex > -1 && attribute.SectionIndex < INIAttribute.Sections.Length ? INIAttribute.Sections[attribute.SectionIndex] : "");

                INISection section = _sections.ContainsKey(sectionName) ? _sections[sectionName] : _sections[sectionName] = new INISection(INIAttribute, sectionName);
                section.AddProperty(property, attribute);

                maxPropertyNameLength = Math.Max(section.MaxPropertyNameLength, maxPropertyNameLength);
            }

            return maxPropertyNameLength;
        }

        public IOrderedEnumerable<INISection> GetSections() {
            return SectionsOrdered;
        }

        public INISection GetSection(string name) {
            return _sections.ContainsKey(name) ? _sections[name] : null;
        }
    }
}