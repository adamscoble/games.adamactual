using System;

namespace AdamActual.Serialization.INI {
    public enum INIPropertyBool { Inherit, True, False }

    [AttributeUsage(INI.PROPERTY_ATTRIBUTE_TARGETS)]
    public class INIPropertyAttribute : Attribute {
        public string Name = null;
        public string Section = null;
        public int SectionIndex = -1;
        public string Comment = null;
        public string InlineComment = null;

        // Bool settings
        public INIPropertyBool BoolSerializeAsString = INIPropertyBool.Inherit;

        // Enum settings
        public INIPropertyBool EnumCommentAppendValues = INIPropertyBool.Inherit;
        public INIPropertyBool EnumCommentAppendInline = INIPropertyBool.Inherit;
        public INIPropertyBool EnumSerializeAsName = INIPropertyBool.Inherit;

        // Array settings
        public INIPropertyBool ArraySerializeMultipleEntries = INIPropertyBool.Inherit;
    }
}