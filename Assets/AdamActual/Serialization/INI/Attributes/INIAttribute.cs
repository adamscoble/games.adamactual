using System;

namespace AdamActual.Serialization.INI {
    public enum MemberSerialization { OptIn, OptOut }
    public enum INIPadType { None, Spaced, Section, Global }

    [AttributeUsage(INI.CLASS_ATTRIBUTE_TARGETS)]
    public class INIAttribute : Attribute {
        public string[] Sections = null;
        public string[] SectionComments = null;
        public string[] SectionInlineComments = null;

        public readonly MemberSerialization MemberSerialization = MemberSerialization.OptIn;

        public bool Properties { get; set; } = true;
        public bool Fields { get; set; } = false;

        public INIPadType PadType { get; set; } = INIPadType.None;

        // Bool settings
        public bool BoolSerializeAsString { get; set; } = false;

        // Enum settings
        public bool EnumCommentAppendValues { get; set; } = true;
        public bool EnumCommentAppendInline { get; set; } = false;
        public bool EnumSerializeAsName { get; set; } = false;

        // Array settings
        public bool ArraySerializeMultipleEntries { get; set; } = false;

        public INIAttribute() { }

        public INIAttribute(MemberSerialization memberSerialization) {
            MemberSerialization = memberSerialization;
        }
    }
}