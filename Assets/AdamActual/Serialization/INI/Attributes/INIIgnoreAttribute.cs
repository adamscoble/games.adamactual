using System;

namespace AdamActual.Serialization.INI {
    [AttributeUsage(INI.PROPERTY_ATTRIBUTE_TARGETS)]
    public class INIIgnoreAttribute : Attribute {
    }
}