using System;
using UnityEngine;
using AdamActual.Compression;

public class Test : MonoBehaviour {
    const string FILE_PATH = @"D:/test.bytes";

    public enum TestEnum { First = -1, Second = 2, Third = 0 }

    public bool IsWriteToFile = true;

    public bool BoolValue;
    public int[] Values = new int[3];
    readonly ICompressedNumber[] _values = {
        new cInt(0, 1),
        new cInt(0, 1),
        new cInt(0, 3)
    };

    public TestEnum EnumValue = TestEnum.First;
    ICompressedNumber _compressedEnum = new cInt(typeof(TestEnum), (int)TestEnum.Second);

    bool _lastIsWriteToFile;

    void Update() {
        if (IsWriteToFile != _lastIsWriteToFile) {
            (IsWriteToFile ? (Action)WriteToFile : ReadFromFile)();

            _lastIsWriteToFile = IsWriteToFile;
        }
    }

    void ReadFromFile() {
        CompressionBuffer compressionBuffer = new CompressionBuffer(FILE_PATH);

        BoolValue = compressionBuffer.ReadBool();

        for (int i = 0; i < _values.Length; i++) {
            compressionBuffer.Read(ref _values[i]);

            Values[i] = _values[i].Value;
        }

        compressionBuffer.Read(ref _compressedEnum);
        EnumValue = (TestEnum)_compressedEnum.Value;

        Debug.Log("Read from file");
        Debug.Log("Length bytes: " + compressionBuffer.ByteCount);
        Debug.Log("Length bits: " + compressionBuffer.BitCount);
    }

    void WriteToFile() {
        CompressionBuffer compressionBuffer = new CompressionBuffer();

        compressionBuffer.Write(BoolValue);

        for (int i = 0; i < _values.Length; i++) {
            _values[i].Value = Values[i];

            compressionBuffer.Write(_values[i]);
        }

        _compressedEnum.Value = (int)EnumValue;
        compressionBuffer.Write(_compressedEnum);

        Debug.Log("Writing to file");
        Debug.Log("Length bytes: " + compressionBuffer.ByteCount);
        Debug.Log("Length bits: " + compressionBuffer.BitCount);

        compressionBuffer.WriteAllBytesToFile(FILE_PATH);
    }
}