using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AdamActual {
    public class ArraySelector<T> {
        readonly T[] _array;
        readonly bool _avoidRepeats;

        readonly List<int> _indices = new List<int>();
        int _previousIndex = -1;

        public ArraySelector(T[] array, bool avoidRepeats = true) {
            _array = array;
            _avoidRepeats = avoidRepeats;
        }

        public T Next(out int index) {
            T next = Next();

            index = _previousIndex;

            return next;
        }

        public T Next() {
            if (_array.Length == 1) { return _array[0]; }

            if (_indices.Count == 0) {
                _indices.AddRange(Enumerable.Range(0, _array.Length));
                Shuffle(_indices);

                if (_avoidRepeats && _previousIndex != -1) {
                    _indices[_indices.IndexOf(_previousIndex)] = _indices[0];
                    _indices[0] = _previousIndex;
                }
            }

            _previousIndex = _indices[_indices.Count - 1];
            _indices.RemoveAt(_indices.Count - 1);

            return _array[_previousIndex];
        }

        static void Shuffle(List<int> list) {
            int n = list.Count;

            while (n > 1) {
                n--;
                int k = Random.Range(0, n + 1);
                int value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}